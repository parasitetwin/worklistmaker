chromPolConverter <- function(chrom, pol)
{
  if(pol == "POS" && chrom == "RP"){
    chromPol = "RP"
  } else if (pol == "NEG" && chrom == "RP"){
    chromPol = "RN"
  } else if (pol == "POS" && chrom == "HILIC"){
    chromPol = "HP"
  } else if (pol =="NEG" && chrom == "HILIC"){
    chromPol = "HN"
  }

  #For AR only
  if(pol == "NEG" && chrom == "NormalPhase"){
    chromPol = "NPNeg"
  }

  return(chromPol)
}
