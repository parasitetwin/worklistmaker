expMRMUI<-function(id){
  ns<-NS(id)

  tagList(
    fluidRow(
      column(
        width=12,
        box(
          title="Load exp.mrm file and template",
          width=12,
          solidHeader = T,

          column(
            width=4,

            fluidRow(
              shinyFilesButton(id=ns('expMRMPath'), "Choose exp.mrm file", title="", multiple=F)
              # selectInput(inputId=ns('instrument'), "", choices=c("SynaptXS", "MRT", "Agilent6550"))
            )
          ),
          column(
            width=4,

            fluidRow(
              shinyFilesButton(id=ns('templPath'), "Choose MRM template file", title="", multiple=F)
            )
          ),
          br(),
          br(),
          column(
            width=12,

            fluidRow(
              uiOutput(ns('render0'))
            )

          ),
          br(),
          column(
            width=12,

            fluidRow(
              uiOutput(ns('render1'))
            )
          ),
          br(),
          br(),
          fluidRow(
            column(
              width=12,

              uiOutput(ns('render2'))
              # uiOutput(ns('render2')),
              # uiOutput(ns('render3'))
            )
          )
        )
      )
    )
  )


}

expMRMServer<-function(id,r){
  moduleServer(
    id,
    function(input, output, session){
      ns<-session$ns

      #######################
      ####Rendering stuff####
      #######################
      ####Render 0####
      output$render0<-renderUI({
        if(is.null(r$expMRM$expMRMPath)){
          tagList(
            tags$b("mrm.exp file selected:")
          )

        } else {
          tagList(
            tags$b("mrm.exp file selected:"),
            tags$b(paste0(r$expMRM$expMRMPath))
          )
        }
      })

      ####Render 1####
      output$render1<-renderUI({
        if(is.null(r$expMRM$templPath)){
          tagList(
            tags$b("Template file selected:")
          )

        } else {
          tagList(
            tags$b("Template file selected:"),
            tags$b(paste0(r$expMRM$templPath))
          )
        }
      })

      ####Render 2####
      output$render2<-renderUI({
        req(r$expMRM$templPath)
        req(r$expMRM$expMRMPath)

        actionButton(ns("makeExpFile"), "Make .exp file")
      })

      #################################################
      #### Handling all parameter inputs from user ####
      #################################################
      ####Input which mrm.exp file to use####
      observeEvent(
        ignoreNULL=TRUE,
        ignoreInit = TRUE,
        eventExpr={
          list(
            input$expMRMPath#,
            # input$refreshButton
          )
        },
        handlerExpr={
          shinyFileChoose(input,'expMRMPath', root=c(r$workListMaker$roots,wd="."), filetypes=c('','exp'), session=session)
          # print(input$tmpFilePath)
          # print(length(grep(".xlsx", as.character(input$tmpFilePath))))
          if(!is.null(input$expMRMPath) && length(grep(".exp", as.character(input$expMRMPath)))>0){
            fileSelMonitor<-parseFilePaths(r$workListMaker$roots,input$expMRMPath)
            r$expMRM$expMRMPath <- as.character(fileSelMonitor$datapath)

            r$expMRM$pathToWriteTo <- dirname(r$expMRM$expMRMPath)

            showNotification(".exp-file loaded successfully!")
          }
        }
      )

      ####Input which mrm.exp file to use####
      observeEvent(
        ignoreNULL=TRUE,
        ignoreInit = TRUE,
        eventExpr={
          list(
            input$templPath#,
            # input$refreshButton
          )
        },
        handlerExpr={
          shinyFileChoose(input,'templPath', root=c(r$workListMaker$roots,wd="."), filetypes=c('','xlsx'), session=session)
          # print(input$tmpFilePath)
          # print(length(grep(".xlsx", as.character(input$tmpFilePath))))
          if(!is.null(input$templPath) && length(grep(".xlsx", as.character(input$templPath)))>0){
            fileSelMonitor<-parseFilePaths(r$workListMaker$roots,input$templPath)
            r$expMRM$templPath <- as.character(fileSelMonitor$datapath)

            showNotification("Template loaded successfully!")
          }
        }
      )

      ####Make .exp file after loading all necessary files ####
      observeEvent(
        ignoreNULL=TRUE,
        ignoreInit = TRUE,
        eventExpr={
          input$makeExpFile
        },
        handlerExpr={
          # progressMRM <- AsyncProgress$new(message="Making new .exp file\n")
          funcOutput <- rewriteExpFile(templPath = r$expMRM$templPath,
                                       expFilePath = r$expMRM$expMRMPath,
                                       outputPath = r$expMRM$pathToWriteTo,
                                       session=session)
          # progressMRM$close()
          if(funcOutput[1] == "Error!")
          {
            shinyalert(funcOutput[1], funcOutput[2], type="error")
          } else {
            shinyalert(funcOutput[1], funcOutput[2], type="success")
          }
        }
      )
    }
  )
}
