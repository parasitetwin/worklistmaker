printTray <- function(saveDir, instrument){
  wb <- createWorkbook()
  addWorksheet(wb,"Tray1")

  trayDF <- data.frame(matrix(ncol=9, nrow=15))
  trayDF[1,1] <- "Name of tray:"
  trayDF[1,2] <- "Tray1"

  trayDF[2,2] <- "1"
  trayDF[2,3] <- "2"
  trayDF[2,4] <- "3"
  trayDF[2,5] <- "4"
  trayDF[2,6] <- "5"
  trayDF[2,7] <- "6"
  trayDF[2,8] <- "7"
  trayDF[2,9] <- "8"

  trayDF[3,1] <- "A"
  trayDF[4,1] <- "B"
  trayDF[5,1] <- "C"
  trayDF[6,1] <- "D"
  trayDF[7,1] <- "E"
  trayDF[8,1] <- "F"

  trayDF[12,1] <- "!!!DOUBLE CHECK THAT ALL VIALS ARE IN POSITIONS AS BELOW!!!"
  trayDF[13,1] <- "External MeOH:"
  trayDF[14,1] <- "External SST:"
  trayDF[15,1] <- "External condPlasma:"
  trayDF[16,1] <- "External ltQCs:"
  trayDF[17,1] <- "External sQCs:"

  trayDF[19,1] <- "Instructions"
  trayDF[20,1] <- "1. Fill in the name of the plate in excel-cell \"1B\""
  trayDF[21,1] <- "2. Put in the name of the sample in the correct well above"
  trayDF[22,1] <- "3. Copy worksheet to add multiple trays and save them all into one xlsx-document"
  trayDF[23,1] <- "4. If sQC or ltQC in another plate/vial in another tray, fill in from left to right in cells"

  trayDF[25,1] <- "Tray formats for diff. instruments for external ltQC, sQC, condPlasma"
  trayDF[26,1] <- "SynaptXS, MRTs: Numbering in trays goes rowwise; e.g. 1-8, 9-16, etc. \"Plate:Number\" ----> Example: \"1:2\""
  trayDF[27,1] <- "Agilent6550: Trays are organized in rows with letters and cols in numbers; A1-A9, B1-B9, etc. \"PPlate-LetterNumber\" ----> Example: \"P1-A5\""

  if(instrument == "SynaptXS"){
    trayDF[13, 2:6] <- c(paste0("1:", seq(1:5)))
    trayDF[14, 2] <- paste0("1:6")
    trayDF[15, 2:3] <- c(paste0("1:", seq(7,8)))
    #trayDF[16, 2:4] <- #c(paste0("1:", seq(9,11)))
    trayDF[17, 2:4] <- c(paste0("1:", seq(9,11)))
  } else if (instrument == "MRT"){
    trayDF[13, 2] <- c(paste0("V:1"))
    trayDF[14, 2] <- paste0("V:2")
    trayDF[15, 2] <- c(paste0("V:3"))
    #trayDF[16, 2:4] <- #c(paste0("1:", seq(9,11)))
    trayDF[17, 2] <- c(paste0("V:4"))
  } else if(instrument == "QTrap"){
    trayDF[13, 2:3] <- c("1","2")
    trayDF[14, 2] <- paste0("3")
    trayDF[15, 2] <- ""
    #trayDF[16, 2:4] <- #c(paste0("1:", seq(9,11)))
    trayDF[17, 2:4] <- c(15, 16, 17)
  }

  writeData(wb, 1, trayDF, colNames=F)

  #Changing color
  boldStyle <- createStyle(textDecoration = "bold")
  cursiveStyle <- createStyle(textDecoration = "italic")
  pinkStyle <- createStyle(fgFill = "#FCE4D6")
  blueStyle <- createStyle(fgFill = "#DDEBF7")
  brownStyle <- createStyle(fgFill = "#BF8F00")
  greenStyle <- createStyle(fgFill = "#DAF7A6")
  redStyle <- createStyle(fgFill = "red")
  bloodRedStyle <- createStyle(fgFill = "darkred")
  textStyle <- createStyle(numFmt="TEXT")

  addStyle(wb, 1, boldStyle, rows=1, cols=1, gridExpand = T, stack = TRUE)
  addStyle(wb, 1, cursiveStyle, rows=1, cols=2, gridExpand = T, stack = TRUE)
  addStyle(wb, 1, pinkStyle, rows=2, cols=c(2:9), gridExpand = T, stack = FALSE)
  addStyle(wb, 1, blueStyle, rows=c(3:8), cols=1, gridExpand = T, stack = FALSE)
  addStyle(wb, 1, brownStyle, rows=c(19:23), cols=c(1:9), gridExpand = T, stack = FALSE)
  addStyle(wb, 1, greenStyle, rows=c(25:27), cols=c(1:9), gridExpand = T, stack = FALSE)
  addStyle(wb, 1, textStyle, rows=c(13:17), cols=c(1:9), gridExpand = T, stack = TRUE)
  addStyle(wb, 1, blueStyle, rows=c(13:17), cols=1, gridExpand=T, stack=TRUE)
  addStyle(wb, 1, redStyle, rows=c(13:17), cols=c(2:6), gridExpand = T, stack=TRUE)
  addStyle(wb, 1, bloodRedStyle, rows=12, cols=c(1:6), gridExpand = T, stack=F)

  setColWidths(wb, sheet=1, cols=1, widths=19)
  setColWidths(wb, sheet=1, cols=2:ncol(trayDF), widths=12)

  saveWorkbook(wb, paste0(saveDir,"/TrayTemplate.xlsx"), overwrite=T)
}
